# Python-Übung 1 - Komplexe Zahlen

Die 4. Python-Übung der Vorlesung Mathematik für Studierende der Chemie II.

## Themen des Übungszettels
* Orthonormalisierung

### Vermittelte Python-Konzepte

* Einführung in Plotten mit matplotlib.

## Nutzung

1. Anmeldung auf [jupyter-cloud](https://jupyter-cloud.gwdg.de) mit den
   studentischen Zugangsdaten (Stud.IP, FlexNow, ...) oder dem GWDG-Account.
2. In der oberen Menüleiste: Reiter **Git** 👉🏿 **Clone a Repository**.
3. Es öffnet sich ein Popup-Dialog. Hier den HTTPS-Clone-Link dieses Projekts eintragen: 
   [https://gitlab.gwdg.de/math-for-chemists-two/exercise-4.git](https://gitlab.gwdg.de/math-for-chemists-two/exercise-4.git)
4. Im neuen Ordner auf das Jupyter-Notebook navigieren.
5. Fertig!

## Autoren

* Marius Herbold
