#!/usr/bin/env python3
# encoding: utf-8

# Import required libraries.
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def plot_vectors(vectors):
    """Plot vectors in 3D."""
    # Define the Cartesian basis vectors for reference.
    c_basis = np.array([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])
    
    # Create a figure.
    fig = plt.figure()
    plot = fig.add_subplot(111, projection='3d')
    
    # Plot the input vectors.
    for vec in vectors:
        plot.quiver(0, 0, 0, vec[0], vec[1], vec[2], color='red')

    # Plot the basis vectors.
    for vec in c_basis:
        plot.quiver(0, 0, 0, vec[0], vec[1], vec[2], color='blue')
  
    # Set title and labels.
    plot.set_title('3D Plot')
    plot.set_xlabel('x-axis')
    plot.set_ylabel('y-axis')
    plot.set_zlabel('z-axis')

    plot.set_xlim(-1.5, 2.5)
    plot.set_ylim(-1.5, 2.5)
    plot.set_zlim(-1.5, 2.5)
  
    # Display the plot.
    plt.show()

def vectors_angle(vec1, vec2):
    """Calculate the angle between two vectors."""
    phi = np.arccos(np.dot(vec1, vec2))
    return phi

# Ethan structure.
C1=[ 0.000,  0.000,  0.000]
C2=[ 0.000,  0.000,  1.450]
H1=[ 1.027,  0.000, -0.363]
H2=[-0.513, -0.889, -0.363]
H3=[-0.513,  0.889, -0.363]
H4=[-1.027,  0.000,  1.813]
H5=[ 0.513, -0.889,  1.813]
H6=[ 0.513,  0.889,  1.813]
ethane = np.array([C1, C2, H1, H2, H3, H4, H5, H6])
ethane_topology = [[0, 1], [0, 2], [0, 3], [0, 4], [1, 5], [1, 6], [1, 7]]

def plot_structure_comparison(structure):
    # Create a figure and axes.
    fig = plt.figure(figsize=(10,5))
    ax = fig.add_subplot(121, projection='3d')
    ax2 = fig.add_subplot(122, projection='3d')

    plot_structure(structure, ax)
    plot_structure(ethane, ax2)

    # Set title and label.
    ax.set_title("Modifizierte Ethanstrukture")
    ax.set_xlabel('x-axis')
    ax.set_ylabel('y-axis')
    ax.set_zlabel('z-axis')

    ax2.set_title("Ausgangs Ethanstrukture")
    ax2.set_xlabel('x-axis')
    ax2.set_ylabel('y-axis')
    ax2.set_zlabel('z-axis')
    
    plt.subplots_adjust(left=0.0, right=1, bottom=0.0, top=1)

    
def plot_structure(structure, ax):
    """Plot the molecular structure of ethane.

    The structural information needs to be provided in the form
    of a numpy array.
    """
    # Plot the atoms.
    count_carbon = 1
    count_hydrogen = 1
    for atom in structure[:2]:
        ax.scatter(atom[0], atom[1], atom[2], s=1000, c='grey')
        ax.text(atom[0], atom[1], atom[2], f'     $C_{count_carbon}$',
                color='r', size=16)

        count_carbon += 1
        
    for atom in structure[2:]:
        ax.scatter(atom[0], atom[1], atom[2], s=500, c='lightgrey')
        ax.text(atom[0], atom[1], atom[2], f'     $H_{count_hydrogen}$',
                color='r',size=16)
        
        count_hydrogen += 1
    
    # Plot the bonds.
    for pair in ethane_topology:
        ax.plot(structure[pair[:], 0], structure[pair[:], 1],
                structure[pair[:],2],
                linewidth='10', color='gray')
